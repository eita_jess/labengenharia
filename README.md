**Projeto AEL** - **A**uxiliar de **E**mpréstimos de **L**ivros para a Matéria Laboratório de Engenharia 
do curso de Análise e Desenvolvimento de Sistemas da FATEC ZL. 
Tarde - 2º Semestre 2016

Projeto realizado por:

- FELIPE LIMA
- FERNANDO MORAES OLVIEIRA
- JESSICA CARNEIRO BATISTA

Professor:

- Antônio Rodrigues de Carvalho Neto

O histórico e a documentação podem ser acompanhados no [wiki do projeto](https://bitbucket.org/eita_jess/labengenharia/wiki/).

A **documentação** deste sistema também se encontra na pasta "[Recursos](https://bitbucket.org/eita_jess/labengenharia/src/d4543119786b2aa79f8484ec2072b91e2d2d5e36/OrganizadorLivros/Recursos/?at=master)" juntamente com as tabelas do banco de dados ([aeldb.sql](https://bitbucket.org/eita_jess/labengenharia/src/d4543119786b2aa79f8484ec2072b91e2d2d5e36/OrganizadorLivros/Recursos/aeldb.sql?at=master&fileviewer=file-view-default)).

**OBS.:** Para a conexão no banco, trocar o usuário e senha do MySQL 
na Classe DBUtil (package edu.projeto.dao).

![Captura de Tela 2016-12-04 às 01.09.57.png](https://bitbucket.org/repo/aEM6Lo/images/3728314634-Captura%20de%20Tela%202016-12-04%20a%CC%80s%2001.09.57.png)